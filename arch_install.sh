sudo pacman -S base-devel git cmake
sudo pacman -S htop lsof strace

sudo pacman -S zsh tmux stow vim fzf ranger w3m pass
sudo pacman -S zip unzip 

sudo pacman -S openssh
sudo systemctl enable sshd.service
sudo systemctl start sshd.service

sudo pacman -S xorg-server xorg-apps xorg-xinit xterm
sudo pacman -S libxkbcommon-x11
# sudo pacman -S xclipboard xclip
sudo pacman -S bspwm sxhkd xdo acpi dmenu rofi compton feh
# (dunst libnotify-bin) gnome-keyring

sudo pacman -S xorg-xfontsel xorg-xlsfonts
#reminder: mkfontscale mkfontdir

sudo yay -S google-drive-ocamlfuse


